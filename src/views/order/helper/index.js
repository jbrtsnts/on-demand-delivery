import UnitTypes from '@/views/order/entity/UnitTypes';
import Categories from '@/views/order/entity/Categories';


export default class OrderHelper {
  static getUnitTypeById (id) {
    return UnitTypes.find(item => item.id === id);
  }

  static getCategoryById (id) {
    if (!id) return;
    try {
      return Categories.find(item => item.id === id)
    } catch (error) {
      return; 
    }
  }

  static getCategoryName (id) {
    const category = this.getCategoryById(id);
    try {
      return `${category.parent} > ${category.name}`
    } catch (error) {
      return 'Other'
    }
  }
}