import moment from 'moment';
export default class CalendarWeek {
  constructor() {
    
    this.data = CalendarWeek.generateCalerdarWeek();

    this.disabledTimeSlots = CalendarWeek.getDisabledDates();
    this.getCalendarDay = CalendarWeek.getCalendarDay;
    this.defaultFormat = CalendarWeek.defaultFormat;
    this.timeSlots = CalendarWeek.timeSlots
  }

  static generateCalerdarWeek () {
    const daysInWeek = 7;
    const data = new Array();
    for (let i = 1; i <= daysInWeek; i++) {
      data.push({
        date: moment().add(i, 'day').format(CalendarWeek.defaultFormat),
        timeSlots: CalendarWeek.timeSlots
      })
    }

    return data;
  }

  static defaultFormat = 'MM-DD-YYYY';
  static timeSlots = [
    { id: "_60kdiqod3", text: "09:00AM - 10:00AM" },
    { id: "_mnwtncts4", text: "10:30AM - 11:30AM" },
    { id: "_ezb3aqaqo", text: "12:00PM - 01:30PM" },
    { id: "_23x24jffu", text: "02:00PM - 03:30PM" },
    { id: "_ucn8ow4ul", text: "04:00PM - 05:30PM" }
  ]

  static getCalendarDay(date) {
    const $date = moment(date, CalendarWeek.defaultFormat);
    return moment(date, this.defaultFormat).calendar(null, {
      // when the date is closer, specify custom values
      lastWeek: "[Last] dddd",
      lastDay: "[Yesterday]",
      sameDay: "[Today]",
      nextDay: "[Tomorrow]",
      nextWeek: "dddd",
      // when the date is further away, use from-now functionality
      sameElse: function (currentDate) {
        if ($date.diff(currentDate, 'days') === 6) return `dddd`;
        return `[${$date.fromNow()}]`;
      }
    });
  }

  static getTimeSlot (id) {
    return CalendarWeek.timeSlots.find(slot => slot.id === id);
  }

  static getDisabledDates() {
    const defaults = {
      '_60kdiqod3': {
        Sunday: { label: 'Lockdown', color: 'danger' },
        Monday: { label: 'Not Available', color: 'grey' },
        Friday: { label: 'Not Available', color: 'grey' },
        Saturday: { label: 'Not Available', color: 'grey' }
      },
      '_mnwtncts4': {
        Sunday: { label: 'Lockdown', color: 'danger' },
        Monday: { label: 'Not Available', color: 'grey' },
        Friday: { label: 'Not Available', color: 'grey' },
        Saturday: { label: 'Not Available', color: 'grey' }
      },
      '_ezb3aqaqo': {
        Sunday: { label: 'Lockdown', color: 'danger' },
        Monday: { label: 'Not Available', color: 'grey' },
        Friday: { label: 'Not Available', color: 'grey' },
        Saturday: { label: 'Not Available', color: 'grey' }
      },
      '_23x24jffu': {
        Sunday: { label: 'Lockdown', color: 'danger' },
        Monday: { label: 'Not Available', color: 'grey' },
        Friday: { label: 'Not Available', color: 'grey' },
      },
      '_ucn8ow4ul': {
        Sunday: { label: 'Lockdown', color: 'danger' },
        Monday: { label: 'Not Available', color: 'grey' }
      },
    }

    const current = moment();
    const format = CalendarWeek.defaultFormat;
    const calendar = this.data || CalendarWeek.generateCalerdarWeek();
    const tomorrow = calendar[0];
    const closingTime = '05:00PM';
    const lastOrder = moment(`${current.format(format)} ${closingTime}`, `${format} HH:mmA`);

    if (current.isSameOrAfter(lastOrder, 'hour')) {
      const getDay = moment(tomorrow.date, format).format('dddd');
      const disableTimeSlotCount = 2;
      const slots = tomorrow.timeSlots;
      for (let i = 0; i < slots.length; i++) {
        const slot = slots[i];
        if (i < disableTimeSlotCount) {
          defaults[slot.id][getDay] = { label: 'Not Available', color: 'grey' }
        } 
      }
    }

    return defaults;
  }
}
