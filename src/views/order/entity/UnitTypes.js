export default [{
  id: 0,
  text: 'pc',
  label: 'piece',
  plural: {
    text: 'pcs',
    label: 'piece'
  }
}, {
  id: 1,
  text: 'g',
  label: 'gram',
  plural: {
    text: 'g',
    label: 'grams'
  }
}, {
  id: 2,
  text: 'kg',
  label: 'kilo',
  plural: {
    text: 'kg',
    label: 'kilos'
  }
}, {
  id: 3,
  text: 'gal',
  label: 'gallon',
  plural: {
    text: 'gals',
    label: 'gallons'
  }
}, {
  id: 4,
  text: 'mL',
  label: 'milliliter',
  plural: {
    text: 'mL',
    label: 'milliliters'
  }
}, {
  id: 5,
  text: 'L',
  label: 'liter',
  plural: {
    text: 'L',
    label: 'liters'
  }
}, {
  id: 6,
  text: 'pkt',
  label: 'packet',
  plural: {
    text: 'pkt',
    label: 'packets'
  }
}, {
  id: 7,
  text: 'bx',
  label: 'box',
  plural: {
    text: 'bx',
    label: 'box'
  }
}]