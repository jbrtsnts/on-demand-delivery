export default [
  {
    "id": "_uc4t3ll2e",
    "parent": "Fresh",
    "name": "Fresh Chicken"
  },
  {
    "id": "_20sd36yvi",
    "parent": "Fresh",
    "name": "Fresh Pork"
  },
  {
    "id": "_n1camo5z1",
    "parent": "Fresh",
    "name": "Fresh Beef"
  },
  {
    "id": "_jve88gvqz",
    "parent": "Fresh",
    "name": "Vegetables"
  },
  {
    "id": "_g4z9wn9ko",
    "parent": "Fresh",
    "name": "Salad"
  },
  {
    "id": "_8x4lyarxo",
    "parent": "Fresh",
    "name": "Fruits"
  },
  {
    "id": "_n09oafwfo",
    "parent": "Frozen",
    "name": "Frozen Chicken"
  },
  {
    "id": "_fngntiqo0",
    "parent": "Frozen",
    "name": "Frozen Pork"
  },
  {
    "id": "_f8fxssgh8",
    "parent": "Frozen",
    "name": "Frozen Seafood"
  },
  {
    "id": "_ckivc67z0",
    "parent": "Frozen",
    "name": "Frozen Fruits & Vegetables"
  },
  {
    "id": "_zdfkz2wug",
    "parent": "Frozen",
    "name": "Others"
  },
  {
    "id": "_f8xqrb5n6",
    "parent": "Cooking",
    "name": "Rice"
  },
  {
    "id": "_h88e74i5z",
    "parent": "Cooking",
    "name": "Sugar"
  },
  {
    "id": "_moayrbyel",
    "parent": "Cooking",
    "name": "Marinade"
  },
  {
    "id": "_rsh18rpgq",
    "parent": "Cooking",
    "name": "Spices & Seasonings"
  },
  {
    "id": "_e04maye35",
    "parent": "Cooking",
    "name": "Oils"
  },
  {
    "id": "_lsbv4h7av",
    "parent": "Cooking",
    "name": "Pasta & Noodles"
  },
  {
    "id": "_f7f4hv4ao",
    "parent": "Cooking",
    "name": "Sauces & Condiments"
  },
  {
    "id": "_bo38l333b",
    "parent": "Baking",
    "name": "Honey, Syrup, & Nectars"
  },
  {
    "id": "_l1z8w5wpn",
    "parent": "Baking",
    "name": "Flour, Starches, & Cocoa"
  },
  {
    "id": "_bt4jc255x",
    "parent": "Baking",
    "name": "Cream, Condensed, & Evaporated"
  },
  {
    "id": "_ssfq74ce0",
    "parent": "Baking",
    "name": "Supplies & Others"
  },
  {
    "id": "_y2i5p94d7",
    "parent": "Baking",
    "name": "Pancake"
  },
  {
    "id": "_yu2p9vl5b",
    "parent": "Canned Goods",
    "name": "Canned Soup, Broth, & Bouillon"
  },
  {
    "id": "_dw312fy06",
    "parent": "Canned Goods",
    "name": "Canned Meat & Seafood"
  },
  {
    "id": "_yxfjibj53",
    "parent": "Canned Goods",
    "name": "Canned Vegetables"
  },
  {
    "id": "_6lv5mvpqj",
    "parent": "Canned Goods",
    "name": "Canned Fruits"
  },
  {
    "id": "_hk3zpgudi",
    "parent": "Dairy & Eggs",
    "name": "Butter"
  },
  {
    "id": "_5qno68j4b",
    "parent": "Dairy & Eggs",
    "name": "Lactose-Free & Soy"
  },
  {
    "id": "_zkhzoamre",
    "parent": "Dairy & Eggs",
    "name": "Yogurt"
  },
  {
    "id": "_88a6vdw2j",
    "parent": "Dairy & Eggs",
    "name": "Eggs"
  },
  {
    "id": "_y4soeoyyq",
    "parent": "Dairy & Eggs",
    "name": "Ice Cream"
  },
  {
    "id": "_ks2kqz3vm",
    "parent": "Dairy & Eggs",
    "name": "Cheese"
  },
  {
    "id": "_8h67jvkc1",
    "parent": "Dairy & Eggs",
    "name": "Milk & Choco Drink"
  },
  {
    "id": "_5by8hkwku",
    "parent": "Beverage",
    "name": "Powdered Mixes"
  },
  {
    "id": "_evuh5eeg1",
    "parent": "Beverage",
    "name": "Water"
  },
  {
    "id": "_tsda8jbyc",
    "parent": "Beverage",
    "name": "Adult Formula Milk"
  },
  {
    "id": "_ke6tti43h",
    "parent": "Beverage",
    "name": "Ready to Drink"
  },
  {
    "id": "_fa00zkgu0",
    "parent": "Beverage",
    "name": "Coffee & Tea"
  },
  {
    "id": "_ro9hb7fhk",
    "parent": "Beverage",
    "name": "Softdrinks"
  },
  {
    "id": "_yb85fqfkh",
    "parent": "Beverage",
    "name": "Energy Drinks"
  },
  {
    "id": "_3hrpvn46w",
    "parent": "Snacks",
    "name": "Pudding and Desserts"
  },
  {
    "id": "_1ueefsi0v",
    "parent": "Snacks",
    "name": "Candies & Gum"
  },
  {
    "id": "_k1wncppnx",
    "parent": "Snacks",
    "name": "Cookies"
  },
  {
    "id": "_03duneilg",
    "parent": "Snacks",
    "name": "Cakes"
  },
  {
    "id": "_lyvhghh4g",
    "parent": "Snacks",
    "name": "Dips"
  },
  {
    "id": "_rj63p0vqk",
    "parent": "Snacks",
    "name": "Cereals & Oats"
  },
  {
    "id": "_2kbgeksuc",
    "parent": "Snacks",
    "name": "Chips & Crisps"
  },
  {
    "id": "_nyb0jxmiv",
    "parent": "Snacks",
    "name": "Chocolates"
  },
  {
    "id": "_5awy6tqcs",
    "parent": "Snacks",
    "name": "Biscuits & Wafers"
  },
  {
    "id": "_ba5egevzt",
    "parent": "Snacks",
    "name": "Instant & Ready to Cook"
  },
  {
    "id": "_j1eyvqfwp",
    "parent": "Snacks",
    "name": "Nuts & Seeds"
  },
  {
    "id": "_680xnt29e",
    "parent": "Bakery",
    "name": "Spreads"
  },
  {
    "id": "_o4qfgcbl3",
    "parent": "Bakery",
    "name": "Buns & Rolls"
  },
  {
    "id": "_bmvt9e4de",
    "parent": "Bakery",
    "name": "Loaf Bread"
  },
  {
    "id": "_uk56qjdu2",
    "parent": "Personal Care",
    "name": "Vitamins & Supplements"
  },
  {
    "id": "_tyghabdiq",
    "parent": "Personal Care",
    "name": "Soap & Body Wash"
  },
  {
    "id": "_k3gutdntg",
    "parent": "Personal Care",
    "name": "Lotion"
  },
  {
    "id": "_2u93j0f5a",
    "parent": "Personal Care",
    "name": "Shaving & Hair Removal"
  },
  {
    "id": "_g2vaj4hyt",
    "parent": "Personal Care",
    "name": "Tissue & Cotton"
  },
  {
    "id": "_fkvxgyjr2",
    "parent": "Personal Care",
    "name": "Patches"
  },
  {
    "id": "_6d0ulxg7q",
    "parent": "Personal Care",
    "name": "Alcohol"
  },
  {
    "id": "_y41jsxza4",
    "parent": "Personal Care",
    "name": "Adult Diapers"
  },
  {
    "id": "_mhkpmns0d",
    "parent": "Personal Care",
    "name": "Gauze and Bandage"
  },
  {
    "id": "_udvlhrzm6",
    "parent": "Personal Care",
    "name": "Antiseptics & Rubs"
  },
  {
    "id": "_xd2xdywod",
    "parent": "Personal Care",
    "name": "Powders"
  },
  {
    "id": "_nv3nsubh9",
    "parent": "Personal Care",
    "name": "Birth Control & Contraceptives"
  },
  {
    "id": "_j1v8ukzgi",
    "parent": "Personal Care",
    "name": "Oral Care"
  },
  {
    "id": "_sx622zegv",
    "parent": "Personal Care",
    "name": "Facial Care"
  },
  {
    "id": "_w7kc3tt9f",
    "parent": "Personal Care",
    "name": "Feminine Care"
  },
  {
    "id": "_pn27zmnbz",
    "parent": "Personal Care",
    "name": "Fragrance & Deodorants"
  },
  {
    "id": "_15nrrmmmz",
    "parent": "Personal Care",
    "name": "Hair Care"
  },
  {
    "id": "_q46swnj6g",
    "parent": "Household Products",
    "name": "Pest Control"
  },
  {
    "id": "_g0zocrfsv",
    "parent": "Household Products",
    "name": "Party Needs"
  },
  {
    "id": "_qmc8xa6ry",
    "parent": "Household Products",
    "name": "Cleaning Products"
  },
  {
    "id": "_zgf47thob",
    "parent": "Household Products",
    "name": "Shoe Care"
  },
  {
    "id": "_3r3yrkw8z",
    "parent": "Household Products",
    "name": "Air Fragrances"
  },
  {
    "id": "_3iypdwzb5",
    "parent": "Household Products",
    "name": "Laundry"
  },
  {
    "id": "_dw8ranjrl",
    "parent": "Household Products",
    "name": "Tissue & Paper Towels"
  },
  {
    "id": "_voqu6ie5j",
    "parent": "Household Products",
    "name": "Dishwashing"
  },
  {
    "id": "_9b6uwptqm",
    "parent": "Baby & Toddler Care",
    "name": "Diapers & Wipes"
  },
  {
    "id": "_v5b4y4z3a",
    "parent": "Baby & Toddler Care",
    "name": "Bath & Body Care"
  },
  {
    "id": "_ahsfzrsno",
    "parent": "Baby & Toddler Care",
    "name": "Food & Formula Milk"
  },
  {
    "id": "_0ukjst1x3",
    "parent": "Pets",
    "name": "Cat Food & Supplies"
  },
  {
    "id": "_kvnihcuml",
    "parent": "Pets",
    "name": "Dog Food & Supplies"
  },
  {
    "id": "_m265vgsfc",
    "parent": "Alcoholic Beverage",
    "name": "Beer"
  },
  {
    "id": "_mtz68o0qo",
    "parent": "Alcoholic Beverage",
    "name": "Liquor"
  },
  {
    "id": "_xzleiw2t3",
    "parent": "Alcoholic Beverage",
    "name": "Wine & Spirits"
  }
]