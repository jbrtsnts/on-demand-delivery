export default class ServiceFees {
  constructor() {
    this.types = ServiceFees.types;
  }

  static types = [{
    id: 1,
    amount: 60,
    amountType: 'amount',
    label: "PHP 60.00" // amount/percent
  }, {
    id: 2,
    amount: 10,
    amountType: 'percent', // amount/percent
    label: '(additional) +10%'
  }]

  static getServiceType(_id) {
    return ServiceFees.types.find(({ id }) => id === _id);
  }
}