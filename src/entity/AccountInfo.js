import PhoneNumber from 'awesome-phonenumber';

export default class AccountInfo {
  constructor(account) {
    this.contactNumber = this.getSmsNumber((account || {}).contactNumber || null);
    this.contactPerson = (account || {}).contactPerson || null;
    this.landmark = (account || {}).landmark || null;
    this.street = (account || {}).street || 'N/a';
    this.suburb = (account || {}).suburb || null;
  }

  get dataField () {
    return {
      contactPerson: {
        value: this.contactPerson,
        label: 'Name'
      },
      contactNumber: {
        value: this.contactNumber,
        label: 'Contact'
      },
      street: {
        value: this.street,
        label: 'Address'
      },
      suburb: {
        value: this.suburb,
        label: 'Brgy'
      },
      landmark: {
        value: this.landmark,
        label: 'Landmark'
      },
    }
  }

  getSmsNumber(contact) {
    if (!contact) return null;
    const pn = new PhoneNumber(contact, 'PH');
    
    return pn.getNumber('e164');
  }
}