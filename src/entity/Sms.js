import PhoneNumber from 'awesome-phonenumber';

export default class Sms {
  constructor(Sms) {
    this.from = (Sms || {}).from || null;
    this.to = this.getSmsNumber((Sms || {}).to || null);
    this.text = (Sms || {}).text || null;
  }

  getSmsNumber(number) {
    if (!number) return null;
    const pn = new PhoneNumber(number);
    return pn.getNumber('e164').substr(1);
  }
}