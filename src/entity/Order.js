import { uid } from '@/plugins/helper.js';
import moment from 'moment';

import AccountInfo from '@/entity/AccountInfo';
import ServiceFees from '@/entity/ServiceFees';

export default class Order {
  constructor(Order = {}) {
    this.id = Order.id || uid();
    this.serviceFee = Order.serviceFee || 0;
    this.deliveryFee = Order.deliveryFee || 0;
    this.deliveryDate = Order.deliveryDate || null;
    this.timeSlotId = Order.timeSlotId || null;
    this.paymentType = Order.paymentType || null,
    this.deliveryDetails = new AccountInfo(Order.deliveryDetails || null);
    this.completed = Order.completed || false; 
    this.count = Order.lineItems ? Order.lineItems.length : Order.count || 0;
    this.totalCost = Order.totalCost || 0;

    const isCreated = (Order || {}).entity && ((Order || {}).entity || {}).dateCreated;
    this.entity = {
      dateCreated: isCreated ? isCreated : moment().utc().format(),
      dateCompleted: this.completed ? moment().utc().format() : null
    }
  }

  getServiceFee() {
    const serviceFee = ServiceFees.getServiceType(this.serviceFee) || {};
    try {
      if (serviceFee.amountType === 'percent') return Math.round((this.totalCost * serviceFee.amount) / 100);
      if (serviceFee.amountType === 'amount') return serviceFee.amount;
      return 'Invalid Amount'; 
    } catch (error) {
      return 'Invalid Amount test'; 
    }
  }

  getSubTotal() {
    const totalCost = parseFloat(this.totalCost);
    const serviceFee = parseFloat(this.getServiceFee);
    const deliveryFee = parseFloat(this.deliveryFee);
    return totalCost + serviceFee + deliveryFee;
  }
}