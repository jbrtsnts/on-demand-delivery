import { uid } from '@/plugins/helper.js'

export default class OrderItem {
  constructor(OrderItem = {}, OrderId) {
    this.orderId = OrderId || null;
    this.id = OrderItem.id || uid();
    this.name = OrderItem.name || null;
    this.quantity = OrderItem.quantity || null;
    this.brand = OrderItem.brand || null;
    this.unitId = OrderItem.unitId || 0;
    this.categoryId = OrderItem.categoryId || null;
    this.notes = OrderItem.notes || null;
  }
}