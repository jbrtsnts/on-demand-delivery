export default class User {
  constructor(User = {}) {
    this.refreshToken = User.refreshToken || null;
    this.uid = User.uid || null;
    this.displayName = User.displayName || null;
    this.photoURL = User.photoURL || null;
    this.email = User.email || null;
    this.emailVerified = User.emailVerified || null;
    this.phoneNumber = User.phoneNumber || null;
    this.isAnonymous = User.isAnonymous || null;
    this.tenantId = User.tenantId || null;
    this.metadata = User.metadata || null;
    this.providerData = User.providerData || null;
  }
}