import Order from '@/entity/Order.js';

export default class CompleteOrder extends Order {
  constructor(Order, lineItems) {
    super(Order);
    this.lineItems = lineItems;
  }
}