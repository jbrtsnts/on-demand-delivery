import unitTypes from '@/views/order/entity/UnitTypes.js';

export default {
  message: 'Hello Hagonoy!',
  authObserved: false,
  order: {
    lineItems: [],
    serviceFee: 0.0,
    deliveryFee: 0.0
  },
  unitTypes: unitTypes,
  orders: []
};
