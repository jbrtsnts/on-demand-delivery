import CompleteOrder from '@/entity/CompleteOrder';
import Storage from './storage';

export default {
  UPDATE_MESSAGE (state, payload) {
    state.message = payload;
  },
  START_AUTH_OBSERVE (state) {
    state.authObserved = true;
  },
  DELETE_ORDER_ITEMS (state, payload) {
    const items = state.order.lineItems;
    for (let i = 0; i < items.length; i++) {
      const line = items[i];
      for (let n = 0; n < payload.length; n++) {
        const o = payload[n];
        if (o.id === line.id) items.splice(i, 1);
      }
    }
    
    Storage.save('lineItems', items);
  },
  UPDATE_ORDER_ITEM (state, payload) {
    const items = state.order.lineItems;
    let updateIndex = -1;
    for (let i = 0; i < items.length; i++) {
      const line = items[i];
      if (line.id === payload.id) updateIndex = i;
    }

    if (updateIndex > -1) {
      items.splice(updateIndex, 1, payload);
    } else {
      items.unshift(payload);
    }

    Storage.save('lineItems', items);
  },
  UPDATE_DELIVERY_SCHEDULE (state, { deliveryDate, timeSlotId }) {
    state.order = {
      ...state.order,
      deliveryDate,
      timeSlotId
    }
  },
  UPDATE_ORDER ( state, { order, lineItems }) {
    state.order = new CompleteOrder({
      ...state.order,
      ...order
    }, lineItems || state.order.lineItems);
  },
  ADD_ORDER ( state, payload) {
    const orders = state.orders;
    for (let i = 0; i < orders.length; i++) {
      const order = orders[i];
      if (order.id === payload.id) return orders.splice(i, 1, payload);
    }
    orders.push(payload);
  },
  STORE_ORDER_DETAILS (state, order) {
    Storage.save('customerInfo', order.deliveryDetails);
  },
  CHECK_FOR_STORAGE (state) {
    const customerInfo = Storage.get('customerInfo');
    const lineItems = Storage.get('lineItems');
    try {
      console.log(lineItems)
      if (customerInfo) state.order.deliveryDetails = JSON.parse(customerInfo);
      if (lineItems) state.order.lineItems = JSON.parse(lineItems);
    } catch (error) {
      console.error('Failed getting storage')
      return;
    }
  },
  CLEAR_STORAGE_ITEMS () {
    Storage.remove('lineItems');
  }
}
