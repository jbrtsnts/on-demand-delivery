import CalendarWeek from  '@/views/order/entity/CalendarWeek';
import CompleteOrder from '@/entity/CompleteOrder'

export default {
  getMessage (state) {
    return state.message;
  },
  isAuthObserved (state) {
    return state.authObserved;
  },
  getSmsReminder(state) {
    const order = new CompleteOrder(state.order);
    const orderId = (order.id || '').substr(1).toUpperCase();
    return (type) => {
      let smsTemplate;
      switch (type) {
        case 'new-order-customer':
          smsTemplate = {
            to: order.deliveryDetails.contactNumber,
            text: `Thanks for trusting ATE PABILI.\n\nOrder Number: ${orderId}\n\nWe received your order and we will get in touch with you shortly, maraming salamat po.`
          }
          break;
        case 'new-order-reminder':
          smsTemplate = {
            to: '+639236782480',
            text: `New Order (${orderId}) placed by ${order.deliveryDetails.contactPerson} and wants to be delivered by ${order.deliveryDate} @ ${CalendarWeek.getTimeSlot(order.timeSlotId).text}.`
          }
          break;
        case 'order-complete':
          smsTemplate = {
            to: order.deliveryDetails.contactNumber,
            text: `Your order is now completed. Please be advise that it will be delivered on ${order.deliveryDate} @ ${CalendarWeek.getTimeSlot(order.timeSlotId).text}.`
          }
          break;
        default:
          smsTemplate = new Object()
          break
      }

      return smsTemplate;
    }
  }
}
