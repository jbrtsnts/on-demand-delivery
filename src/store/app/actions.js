import api from '@/config/api';
import Order from '@/entity/Order'
import OrderItem from '@/entity/OrderItem'

import { auth, database, functions } from 'firebase/app';
import router from '@/router.js';

import Sms from '@/entity/Sms';

export default {
  updateMessage({ commit }, data) {
    commit('UPDATE_MESSAGE', data);
  },
  
  // AUTH
  async login({ commit }, { email, password }) {
    return await auth().signInWithEmailAndPassword(email, password)
    .then(response => {
      commit('UPDATE_USER_INFO', response.user, { root: true });
      return true;
    })
    .catch(error => Promise.reject(error));
  },
  async logout({ commit }) {
    const result = await auth().signOut().then(() => true);
    if (result) commit('UPDATE_USER_INFO', null, { root: true });
    return result;
  },
  observeAuthState({ commit }) {
    return new Promise((resolve) => {
      auth().onAuthStateChanged(user => {
        commit('START_AUTH_OBSERVE');
        commit('UPDATE_USER_INFO', user, { root: true });
        resolve();
      });
    })
  },

  // RESOLVE
  /* eslint no-unused-vars: 0 */
  async resolveRoute(context, route) {
    let resolvedRoute;
    const isAuthObserved = context.getters['isAuthObserved'];
    if (!isAuthObserved) await context.dispatch('observeAuthState');

    const activeUser = context.rootState.AppActiveUser;    
    const to = route || router.currentRoute;

    if (to.matched.some(record => record.meta.auth) && !activeUser) {
      resolvedRoute = {
        path: '/login',
        query: { redirect: to.fullPath }
      }
      if (to.name === 'home') {
        resolvedRoute = {
          path: '/order'
        } 
      }
    } else if (activeUser && to.name === 'page-login') {
      resolvedRoute = {
        path: to.query.redirect || '/'
      }
    }
    
    if (!route && resolvedRoute) return router.push(resolvedRoute);
    return resolvedRoute;
  },

  // PUBLIC ORDER MODULE
  deleteOrderItems({ commit }, array) {
    commit('DELETE_ORDER_ITEMS', array);
  },
  submitOrderItem({ commit }, orderItem) {
    commit('UPDATE_ORDER_ITEM', orderItem);
  },
  selectSchedule({ commit }, payload) {
    commit('UPDATE_DELIVERY_SCHEDULE', payload);
  },
  async updateOrder ({ commit }, order) {
    commit('UPDATE_ORDER', { order });
  },
  async clearOrder ({ commit }) {
    commit('CLEAR_STORAGE_ITEMS');
    commit('UPDATE_ORDER', {
      order: new Order(),
      lineItems: []
    });
    return true;
  },
  async submitOrder({ dispatch, state, commit, getters }, completeOrder) {
    const $db = database();
    const OrderEntity = new Order(completeOrder || state.order);
    await $db.ref(`${api.orders}/${OrderEntity.id}`)
      .set(OrderEntity);
    
    const items = state.order.lineItems;
    for (let i = 0; i < items.length; i++) {
      const OrderItemEntity = new OrderItem(items[i], OrderEntity.id);
      await $db.ref(`${api.orderItems}/${OrderItemEntity.id}`)
        .set(OrderItemEntity);
    }

    if (!completeOrder) { // New Order
      const smsTemplateCustomer =  getters.getSmsReminder('new-order-customer');
      const smsTemplateReminder =  getters.getSmsReminder('new-order-reminder');
      await dispatch('sendSmsReminder', {
        from: 'AtePabili',
        ...smsTemplateReminder,
      })
      await dispatch('sendSmsReminder', {
        from: 'AtePabili',
        ...smsTemplateCustomer,
      });
      commit('STORE_ORDER_DETAILS', OrderEntity);
      return await dispatch('clearOrder');
    }

    commit('UPDATE_ORDER', {
      order: OrderEntity,
      lineItems: completeOrder.lineItems
    });
    return true;
  },
  fetchOrders({ commit }) {
    commit('CHECK_FOR_STORAGE')
    return new Promise(resolve => {
      const $db = database();
      var ordersReference = $db.ref(api.orders).orderByChild('deliveryDate');
      ordersReference.on('value', snapshot => {
        snapshot.forEach(function(childSnapshot) {
          commit('ADD_ORDER', new Order(childSnapshot.val()));
        });
        resolve();
      });
    });
  },

  fetchOrder({ commit }, id) {
    return new Promise(resolve => {
      const $db = database();
      const orderRef = $db.ref(`${api.orders}/${id}`);
      orderRef.once('value').then(snapshot => {
        const order = new Order(snapshot.val());
        const orderItemsRef = $db.ref(api.orderItems);
        orderItemsRef.orderByChild('orderId').equalTo(order.id).once('value').then(snapshot => {
          const lineItems = new Array();
          const items = snapshot.val();
          for (const key in items) {
            if (items.hasOwnProperty(key)) {
              lineItems.push(new OrderItem(items[key], order.id));
            }
          }
          commit('UPDATE_ORDER', { order, lineItems });
          resolve(true);
        })
      })
    });
  },

  sendSmsReminder(context, obj) {
    return new Promise(resolve => {
      const txt = new Sms(obj);
      const sendSms = functions().httpsCallable('sendSms');
      sendSms(txt).then(result => {
        console.log(result);
        resolve(true);
      })
    })
  }
}
