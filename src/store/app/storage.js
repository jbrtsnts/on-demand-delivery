export default class Storage {
  constructor() {

  }

  static save(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  static get(key) {
    return localStorage.getItem(key);
  }

  static remove(key) {
    localStorage.removeItem(key);
  }
}