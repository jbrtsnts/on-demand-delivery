/*=========================================================================================
  File Name: store.js
  Description: Vuex store
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Vuex from 'vuex'

import state from "./state"
import getters from "./getters"
import mutations from "./mutations"
import actions from "./actions"

Vue.use(Vuex)

// Modules
import app from './app';

const store = new Vuex.Store({
  getters,
  mutations,
  state,
  actions,
  modules: {
    app
  },
  strict: process.env.NODE_ENV !== 'production'
})

store.watch((state) => state.AppActiveUser, (oldValue, newValue) => {
  if (oldValue && newValue) return;
  store.dispatch('app/resolveRoute');
});

export default store;
