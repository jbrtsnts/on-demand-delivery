export function uid(preset) {
  let id =
    '_' +
    Math.random()
      .toString(36)
      .substr(2, 9);
  if (preset) id = preset + id;
  return id;
}

export function debounce(fn, wait = 1) {
  let timeout;
  return function (...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => fn.call(this, ...args), wait);
  }
}