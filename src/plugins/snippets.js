/* eslint no-unused-vars: 0 */
import moment from 'moment';
import { Validator } from 'vee-validate';
import PhoneNumber from 'awesome-phonenumber';

export default {
  install(Vue) {
    Vue.prototype.$moment = moment;
    
    Array.prototype.groupBy = function (key) {
      return this.reduce((objectsByKeyValue, obj) => {
        const value = eval(`obj.${key}`);
        objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
        return objectsByKeyValue;
      }, {})
    }

    /**
     * @global $randomId
     * @param {number} [preset] - The preset to be used with randomised ID
     */
    Vue.prototype.$randomId = preset => {
      let id =
        '_' +
        Math.random()
          .toString(36)
          .substr(2, 9);
      if (preset) id = preset + id;
      return id;
    };

    /**
     * @global $randomisedVsSelectAutofill
     * @param {component} [component] - The components reference where the input is belong
     */
    Vue.prototype.$randomisedVsSelectAutofill = component => {
      if (component)
        component.$refs.inputselect.setAttribute(
          'autocomplete',
          component.$randomId()
        );
    };

    const appLoading = document.getElementById('loading-bg');
    Vue.prototype.$loading = class {
      static show() {
        appLoading.style.display = 'block';
      }

      static done() {
        appLoading.style.display = 'none';
      }
    }

    Vue.mixin({
      data() {
        return {
          isMounted: false
        }
      },
      computed: {
        activeUser: $ctrl => $ctrl.$store.state.AppActiveUser
      },
      beforeCreate() {
        const $ctrl = this;
        this.$todo = () => {
          this.$vs.notify({
            color: 'danger',
            title: 'Todo',
            text: "This feature hasn't been done."
          });
        };

        try {
          if (!this.$vxPopup) this.$vxPopup = this.$root.$main.$refs.vxPopup;
        } catch (error) {
          this.$vxPopup = null;
        }
      },
      created() {},
      destroyed() {},
      mounted() {
        this.isMounted = true
      }
    });

    Vue.directive('init', {
      bind(el, binding, vnode) {
        eval(`vnode.context.${binding.expression.replace('this', 'vnode.context')}`);
      }
    })

    Vue.filter('price', function (value) {
      if (isNaN(parseFloat(value))) {
        return value;
      }
      var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP',
        minimumFractionDigits: 2
      });
      return formatter.format(value);
    });

    Validator.extend('phonenumber', {
      getMessage: field => 'The ' + field + ' value is not valid',
      validate: (value,  { countryCode } = {}) => {
        const pn = new PhoneNumber(value, countryCode);
        return pn.isValid();
      }
    }, {
      paramNames: ['countryCode']
    });
  }
}
