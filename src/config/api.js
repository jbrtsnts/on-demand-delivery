const prod = process.env.NODE_ENV === 'production';

export default {
  baseUrl: '',
  orders: prod ? 'app/orders' : 'test-app/orders',
  orderItems: prod ? 'app/order-items' : 'test-app/order-items'
}
